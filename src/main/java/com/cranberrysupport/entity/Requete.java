package com.cranberrysupport.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Requete {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "requete_id")
	private Long id;

	private String sujet = "";
	private String description = "";
	private File file;

	@ManyToOne(targetEntity = Utilisateur.class)
	private Utilisateur client;

	@ManyToOne(targetEntity = Technicien.class)
	private Technicien technicien;

	@Enumerated(EnumType.STRING)
	private Statut statut = Statut.OUVERT;

	@Enumerated(EnumType.STRING)
	private Categorie categorie;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "requete", targetEntity = Commentaire.class)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@Fetch(FetchMode.SELECT)
	private List<Commentaire> listeCommentaires = new ArrayList<>();

	public Requete(String sujet, String descrip, Utilisateur client, Categorie categorie) {
		this.client = client;
		this.sujet = sujet;
		this.description = descrip;
		this.categorie = categorie;
	}

	public void addCommentaire(Commentaire suivant) {
		listeCommentaires.add(suivant);
	}

	public void setTechnicien(Utilisateur tech) {
		this.technicien = (Technicien) tech;
	}
}