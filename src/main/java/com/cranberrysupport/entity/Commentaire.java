package com.cranberrysupport.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Commentaire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String commentaire;

	@ManyToOne(optional = false, targetEntity = Utilisateur.class)
	@JoinColumn(name = "utilisateur_id", nullable = false, updatable = false)
	private Utilisateur auteur;

	@ManyToOne(optional = false, targetEntity = Requete.class)
	@JoinColumn(name = "requete_id", nullable = false)
	private Requete requete;

	public Commentaire(String commentaire, Utilisateur auteur) {
		this.commentaire = commentaire;
		this.auteur = auteur;
	}

	public String toString() {
		return auteur.getNomUtilisateur() + ": " + commentaire + "\n";
	}
}