package com.cranberrysupport.entity;

public enum Statut {

    OUVERT("ouvert"),
    EN_TRAITEMENT("en traitement"),
    FINAL_ABANDON("Abandon"),
    FINAL_SUCCES("Succès");
	

    private String valeur;

    private Statut(String s) {
        this.valeur = s;
    }

    public String getValueString() {
        return valeur;
    }

    public static Statut fromString(String text) {
        if (text == null) return null;
        for (Statut statut: values()) {
            if (statut.getValueString().equals(text)) {
                return statut;
            }
        }
        return null;
    }

    public static String toString(Statut statut) {
        return "Statut " + statut.getValueString() + ": ";
    }
}

