package com.cranberrysupport.entity;

public enum Categorie {

    POSTE_DE_TRAVAIL("Poste de travail"),
    SERVEUR("Serveur"),
    SERVICE_WEB("Service web"),
    COMPTE_USAGER("Compte usager"),
    AUTRE("Autre");

    private String value;

    Categorie(String s) {
        this.value = s;
    }

    public String getValueString() {
        return value;
    }
    public static Categorie fromString(String text) {
        if (text == null) return null;
        for (Categorie categorie : values()) {
            if (categorie.getValueString().equals(text)) {
                return categorie;
            }
        }
        return null;
    }
}
