package com.cranberrysupport.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue("client")
@NoArgsConstructor
@ToString(callSuper = true)
public class Client extends Utilisateur {

	@Transient
	private List<Requete> clientRequestList = new ArrayList<>();

	public Client(String prenom, String nom, String nomUtilisateur, String mdp) {
		super(prenom, nom, nomUtilisateur, mdp, "client");
	}

	public String getRole() {
		return getClass().getSimpleName().toLowerCase();
	}

	@Override
	public void ajoutRequete(Requete nouvelle) {
		clientRequestList.add(nouvelle);
	}

	@Override
	public List<Requete> getListeRequetes() {
		return clientRequestList;
	}

}