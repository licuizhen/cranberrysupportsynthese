package com.cranberrysupport.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "disc")
@AllArgsConstructor
@NoArgsConstructor
public abstract class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String prenom;
	private String nomUtilisateur;
	private String mdp;
	private String role;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "client", targetEntity = Requete.class, fetch = FetchType.EAGER)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<Requete> listeRequetes = new ArrayList<>();

	public Utilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
		this.prenom = prenom;
		this.nom = nom;
		this.nomUtilisateur = nomUtilisateur;
		this.mdp = mdp;
	}

	public void ajoutRequete(Requete nouvelle) {
		listeRequetes.add(nouvelle);
	}

}