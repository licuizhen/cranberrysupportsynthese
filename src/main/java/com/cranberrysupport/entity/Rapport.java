package com.cranberrysupport.entity;

import com.cranberrysupport.controller.Controller;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Rapport {

	private Controller controller = Controller.getInstance();

	public String makeRapport() {
		String rapport = "";
		for (Technicien tech : controller.getTechniciens()) {
			rapport += tech.getNomUtilisateur() + ":\n";
			rapport += getRequeteParStatut(tech) + "\n";
		}
		return rapport;
	}

	private String getRequeteParStatut(Technicien tech) {
		Map<Statut, Integer> statutCounter = new HashMap<>();
		for (Statut statut : Statut.values()) {
			statutCounter.put(statut, 0);
		}

		for (Requete requete : controller.getRequestByTechnician(tech)) {
			Integer count = statutCounter.get(requete.getStatut());
			statutCounter.put(requete.getStatut(), count + 1);
		}
		return formatRequestStringReturn(statutCounter);
	}

	private String formatRequestStringReturn(Map<Statut, Integer> statutCounter) {
		return statutCounter.entrySet().stream().map(entry -> Statut.toString(entry.getKey()) + entry.getValue())
				.collect(Collectors.joining("\n"));
	}

}
