package com.cranberrysupport.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue("technicien")
@NoArgsConstructor
@ToString(callSuper = true)
public class Technicien extends Utilisateur {

	public Technicien(String prenom, String nom, String nomUtilisateur, String mdp) {
		super(prenom, nom, nomUtilisateur, mdp, "technicien");
	}

	@Override
	public String getRole() {
		return getClass().getSimpleName().toLowerCase();
	}
}