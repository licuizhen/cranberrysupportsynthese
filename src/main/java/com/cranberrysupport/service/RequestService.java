package com.cranberrysupport.service;

import java.util.List;

import org.hibernate.Session;

import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Statut;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.util.HibernateUtil;

public class RequestService {

	public List<Requete> getUserRequest(Utilisateur user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Requete> requests = session.createQuery("from Requete where client = " + user.getId(), Requete.class)
				.getResultList();
		session.getTransaction().commit();
		return requests;
	}

	public List<Requete> getRequestByTechnician(Utilisateur user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Requete> requetes = session.createQuery("from Requete WHERE technicien = " + user.getId(), Requete.class)
				.getResultList();
		session.getTransaction().commit();
		return requetes;
	}

	public void addCommentToRequest(Requete requete, Commentaire commentaire) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		commentaire.setRequete(requete);
		requete.addCommentaire(commentaire);
		session.save(commentaire);
		session.saveOrUpdate(requete);

		session.getTransaction().commit();
	}

	public List<Requete> getAssignedRequests(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Requete> requests = session.createQuery("from Requete where technicien = " + id, Requete.class)
				.getResultList();

		session.getTransaction().commit();
		return requests;
	}

	public List<Requete> getAvailableRequests() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Requete> requests = session.createQuery("from Requete where technicien = null", Requete.class)
				.getResultList();
		session.getTransaction().commit();
		return requests;
	}

	public void assignTechToRequest(Requete requete, Utilisateur technicien) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		requete.setTechnicien(technicien);
		requete.setStatut(Statut.EN_TRAITEMENT);
		technicien.ajoutRequete(requete);
		session.saveOrUpdate(requete);
		session.saveOrUpdate(technicien);
		session.getTransaction().commit();
	}

	public void addNewRequest(Requete requete) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Utilisateur creator = requete.getClient();
		session.save(requete);
		creator.ajoutRequete(requete);
		session.saveOrUpdate(creator);
		session.getTransaction().commit();
	}

	public void updateFile(Requete requete) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Utilisateur creator = requete.getClient();
		session.saveOrUpdate(requete);
		creator.ajoutRequete(requete);
		session.saveOrUpdate(creator);
		session.getTransaction().commit();
	}
}
