package com.cranberrysupport.service;

import java.util.List;

import org.hibernate.Session;

import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.util.HibernateUtil;

public class UserService {

	public Utilisateur getUser(String username) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Utilisateur user = session
				.createQuery("from Utilisateur where nomUtilisateur = '" + username + "'", Utilisateur.class)
				.uniqueResult();
		session.getTransaction().commit();
		return user;
	}

	public boolean isLoginValid(Utilisateur user, String password) {
		return user.getMdp().equals(password);
	}

	public Technicien getTechnicien(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Technicien user = session
				.createNativeQuery("select * from Utilisateur u where u.disc = 'technicien' and u.id = " + id,
						Technicien.class)
				.uniqueResult();
		session.getTransaction().commit();
		return user;
	}

	public List<Technicien> getTechniciens() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Technicien> users = session
				.createQuery("from Technicien", Technicien.class)
				.getResultList();
		session.getTransaction().commit();
		return users;
	}

}
