package com.cranberrysupport.service;

import java.util.List;

import org.hibernate.Session;

import com.cranberrysupport.entity.Categorie;
import com.cranberrysupport.entity.Client;
import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Statut;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.util.HibernateUtil;

public class PeuplerService {
	public static void run() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		List<Utilisateur> ps = session.createQuery("FROM Utilisateur", Utilisateur.class).getResultList();

		if (ps.isEmpty()) {
			Utilisateur c1 = new Client("Anni", "Li", "al", "al");
			session.saveOrUpdate(c1);
			Utilisateur c2 = new Client("Cynthia", "Deples", "cyn", "cyn");
			session.saveOrUpdate(c2);
			Utilisateur c3 = new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool");
			session.saveOrUpdate(c3);
			Utilisateur c4 = new Client("Pauline", "St-Onge", "popoline", "monchatkiki");
			session.saveOrUpdate(c4);

			Utilisateur tech1 = new Technicien("George", "WasMarchitain", "geo", "geo");
			session.saveOrUpdate(tech1);
			Utilisateur tech2 = new Technicien("Benjamin", "Claveau", "bc", "bc");
			session.saveOrUpdate(tech2);
			Utilisateur tech3 = new Technicien("Louise", "Boisvert", "techguest", "password");
			session.saveOrUpdate(tech3);

			Requete r1 = new Requete("Exemple 1", "Je ne suis pas content du tout.", c1, Categorie.COMPTE_USAGER);
			r1.setStatut(Statut.EN_TRAITEMENT);
			r1.setTechnicien(tech2);
			tech2.ajoutRequete(r1);
			c1.ajoutRequete(r1);

			session.saveOrUpdate(r1);
			session.saveOrUpdate(tech2);
			session.saveOrUpdate(c1);

			Requete r2 = new Requete("Exemple 2", "As-tu un problème contre moi?", c2, Categorie.POSTE_DE_TRAVAIL);
			c2.ajoutRequete(r2);
			session.saveOrUpdate(r2);
			session.saveOrUpdate(c2);

			Requete r3 = new Requete("Requete technicien 1", "Ca ne marche pas.", tech2, Categorie.SERVEUR);
			r3.setStatut(Statut.EN_TRAITEMENT);
			tech2.ajoutRequete(r3);
			session.saveOrUpdate(r3);
			session.saveOrUpdate(tech2);

			Requete r4 = new Requete("Testing", "Ceci est un test.", tech2, Categorie.SERVICE_WEB);
			tech2.ajoutRequete(r4);
			session.saveOrUpdate(r4);
			session.saveOrUpdate(tech2);

			Requete r5 = new Requete("Consensus", "Sans non, c'est oui.", c3, Categorie.AUTRE);
			r4.setStatut(Statut.EN_TRAITEMENT);
			r5.setTechnicien(tech3);
			tech3.ajoutRequete(r5);
			c3.ajoutRequete(r5);
			
			session.saveOrUpdate(c3);
			session.saveOrUpdate(r5);
			session.saveOrUpdate(tech3);

			Commentaire comm1 = new Commentaire("Commentaire non pertinent.", c1);
			comm1.setRequete(r4);
			r4.addCommentaire(comm1);
			session.saveOrUpdate(comm1);
			session.saveOrUpdate(r4);

			Commentaire comm2 = new Commentaire("This is a trap!", c2);
			comm2.setRequete(r3);
			r3.addCommentaire(comm2);
			session.saveOrUpdate(comm2);
			session.saveOrUpdate(r3);

			Commentaire comm3 = new Commentaire("Sans oui, c'est non.", c2);
			comm3.setRequete(r5);
			r5.addCommentaire(comm3);
			session.saveOrUpdate(comm3);
			session.saveOrUpdate(r5);

			Commentaire comm4 = new Commentaire("Le test n'a pas marché..", tech2);
			comm4.setRequete(r4);
			r4.addCommentaire(comm4);
			session.saveOrUpdate(comm4);
			session.saveOrUpdate(r4);
			
			session.getTransaction().commit();
			
		}
	}
}
