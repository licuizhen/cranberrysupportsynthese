package com.cranberrysupport.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.service.RequestService;
import com.cranberrysupport.service.UserService;

public class Controller {
	private static final String PATH_TO_SAVE_FILE = "dat/";
	private static Controller self = null;
	private RequestService rs = new RequestService();
	private UserService us = new UserService();

	private Controller() {
	}

	public static Controller getInstance() {
		return self == null ? new Controller() : self;
	}

	public Utilisateur getUser(String username) {
		return us.getUser(username);
	}

	public boolean isLoginValid(Utilisateur user, String password) {
		return us.isLoginValid(user, password);
	}

	public List<Requete> getUserRequest(Utilisateur user) {
		return rs.getUserRequest(user);
	}

	public void addCommentToRequest(Requete requete, Commentaire commentaire) {
		rs.addCommentToRequest(requete, commentaire);
	}

	public Technicien getTechnicien(Long id) {
		return us.getTechnicien(id);
	}

	public List<Technicien> getTechniciens() {
		return us.getTechniciens();
	}

	public List<Requete> getAssignedRequests(Long id) {
		return rs.getAssignedRequests(id);
	}

	public List<Requete> getAvailableRequests() {
		return rs.getAvailableRequests();
	}

	public void assignTechToRequest(Requete req_id, Utilisateur user_id) {
		rs.assignTechToRequest(req_id, user_id);
	}

	public void addNewRequest(Requete requete) {
		rs.addNewRequest(requete);
	}

	public void updateRequest(Requete requete) {
		rs.updateFile(requete);
	}

	public List<Requete> getRequestByTechnician(Utilisateur user) {
		return rs.getRequestByTechnician(user);
	}

	public void fileUpload(File fileUploaded, String classname, Requete requete) {
		File file = null;

		try {
			file = new File(PATH_TO_SAVE_FILE + fileUploaded.getName());
			InputStream srcFile = new FileInputStream(fileUploaded);
			OutputStream newFile = new FileOutputStream(file);
			byte[] buf = new byte[4096];
			int len;
			while ((len = srcFile.read(buf)) > 0) {
				newFile.write(buf, 0, len);
			}
			srcFile.close();
			newFile.close();
		} catch (IOException ex) {
			Logger.getLogger(classname).log(Level.SEVERE, null, ex);
		}
		requete.setFile(file);
		updateRequest(requete);
	}
}