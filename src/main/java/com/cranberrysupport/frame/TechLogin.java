package com.cranberrysupport.frame;

import com.cranberrysupport.controller.Controller;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class TechLogin extends JFrame {

	private Controller controller = Controller.getInstance();

	private JTextField passwordInput;
	private JTextField usernameInput;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JButton buttonOK;
	private JButton buttonCancel;
	private GroupLayout layout;

	public TechLogin() {
		initComponents();
		this.setVisible(true);
	}

	private void initComponents() {
		usernameLabel = new JLabel(USERNAME_LABEL);
		passwordLabel = new JLabel(PASSWORD_LABEL);
		buttonOK = new JButton(OK);
		buttonCancel = new JButton(CANCEL);

		usernameInput = new JTextField();
		passwordInput = new JTextField();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		buttonOK.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				okBtnMouseClicked();
			}
		});

		buttonCancel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				annulerBtnMouseClicked();
			}
		});

		layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		createHorizontalLayout();
		createVerticalLayout();

		pack();
	}

	private void createVerticalLayout() {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(33, 33, 33)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(usernameLabel)
								.addComponent(usernameInput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(passwordLabel)
								.addComponent(passwordInput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(buttonOK).addComponent(buttonCancel))
						.addContainerGap(23, Short.MAX_VALUE)));
	}

	private void createHorizontalLayout() {
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addGap(38, 38, 38)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(usernameLabel)
						.addComponent(passwordLabel))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addComponent(buttonOK, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addGap(18, 18, 18).addComponent(buttonCancel))
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addComponent(passwordInput)
								.addComponent(usernameInput, GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)))
				.addContainerGap(49, Short.MAX_VALUE)));
	}

	private void okBtnMouseClicked() {
		Utilisateur potentiel = controller.getUser(usernameInput.getText());
		if (potentiel == null) {
			new ErrorLoginFrame();
		} else if (isTechnicianCanLog(potentiel)) {
			new TechFrame(potentiel);
		} else {
			new ErrorLoginFrame();
		}
		this.setVisible(false);
	}

	private boolean isTechnicianCanLog(Utilisateur potentiel) {
		return controller.isLoginValid(potentiel, passwordInput.getText())
				&& potentiel.getRole().equals(Technicien.class.getSimpleName().toLowerCase());
	}

	private void annulerBtnMouseClicked() {
		this.setVisible(false);
		new MainMenuFrame();
	}
}
