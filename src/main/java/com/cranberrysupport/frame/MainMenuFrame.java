package com.cranberrysupport.frame;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class MainMenuFrame extends JFrame {

	private JFrame jFrame;
	private JLabel demarrageLabel;
	private JButton buttonTechnicien;
	private JButton buttonClient;

	public MainMenuFrame() {
		initComponents();
		this.setVisible(true);
	}

	private void initComponents() {
		jFrame = new JFrame(STARTING);
		demarrageLabel = new JLabel(CHOOSING_LOGIN_TYPE);
		buttonTechnicien = new JButton(TECHNICIAN);
		buttonClient = new JButton(CLIENT);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		GroupLayout groupLayout = new GroupLayout(jFrame.getContentPane());
		jFrame.getContentPane().setLayout(groupLayout);
		setupGroups(groupLayout);

		setupButtons();

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		createHorizontalGroup(layout);
		createVerticalGroup(layout);
		pack();
	}

	private void setupGroups(GroupLayout groupLayout) {
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(GAP_0, GAP_400, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(GAP_0, GAP_300, Short.MAX_VALUE));
	}

	private void setupButtons() {
		buttonTechnicien.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				loginAsTechnician();
			}
		});

		buttonClient.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				loginAsClient();
			}
		});
	}

	private void createVerticalGroup(GroupLayout layout) {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(demarrageLabel).addComponent(buttonClient).addComponent(buttonTechnicien))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}

	private void createHorizontalGroup(GroupLayout layout) {
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(GAP_35, GAP_35, GAP_35).addComponent(demarrageLabel)
						.addGap(GAP_35, GAP_35, GAP_35).addComponent(buttonClient).addGap(GAP_18, GAP_18, GAP_18)
						.addComponent(buttonTechnicien).addContainerGap(GAP_42, Short.MAX_VALUE)));
	}

	private void loginAsTechnician() {
		this.setVisible(false);
		new TechLogin();
	}

	private void loginAsClient() {
		this.setVisible(false);
		new ClientLogin();
	}

}
