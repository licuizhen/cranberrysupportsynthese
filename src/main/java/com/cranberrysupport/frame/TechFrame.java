package com.cranberrysupport.frame;

import java.awt.Desktop;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import com.cranberrysupport.controller.Controller;
import com.cranberrysupport.entity.Categorie;
import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Rapport;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Statut;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class TechFrame extends JFrame {
	private Controller controller = Controller.getInstance();
	private Technicien utilisateur;
	private List<Requete> assignedRequests;
	private List<Requete> availableRequests;
	private List<Technicien> technicians;
	private Requete selectedRequest;
	private DefaultListModel<String> assignedRequestsList = new DefaultListModel<>();
	private DefaultListModel<String> availableRequestList = new DefaultListModel<>();
	private DefaultListModel<String> technicianList = new DefaultListModel<>();

	private JComboBox<String> dropDownCategories;
	private JTextField addCommentBox;
	private JTextArea commentsArea;
	private JTextArea descriptionArea;
	private JFileChooser fileChooser;
	private JLabel fileLabel;
	private JComboBox<String> dropDownFinalize;
	private JButton buttonTakeRequest;
	private JList<String> assignRequestComponent;
	private JList<String> availableRequestBox;
	private JTextArea requestArea;
	private JList<String> supportList;

	public TechFrame(Utilisateur tech) {
		initComponents();
		this.setVisible(true);
		initList(tech);
		makeAssignedRequestList();
		makeAvailableResquestList();
		makeTechnicianList();
	}

	private void initList(Utilisateur tech) {
		utilisateur = controller.getTechnicien(tech.getId());
		assignedRequests = controller.getAssignedRequests(utilisateur.getId());
		availableRequests = controller.getAvailableRequests();
		technicians = controller.getTechniciens();
		technicians.remove(utilisateur);
	}

	private void makeAssignedRequestList() {
		if (assignedRequests.isEmpty()) {
			assignRequestComponent.setModel(noContentToShow("requêtes"));
		} else {
			for (Requete value : assignedRequests) {
				assignedRequestsList.addElement(value.getSujet());
			}
			assignRequestComponent.setModel(assignedRequestsList);
		}
	}

	private void makeAvailableResquestList() {
		if (availableRequests.isEmpty()) {
			availableRequestBox.setModel(noContentToShow("requêtes"));
			buttonTakeRequest.setEnabled(false);
		} else {
			for (Requete value : availableRequests) {
				availableRequestList.addElement(value.getSujet());
			}
			availableRequestBox.setModel(availableRequestList);
			buttonTakeRequest.setEnabled(true);

		}
	}

	private void makeTechnicianList() {
		if (technicians.isEmpty()) {
			supportList.setModel(noContentToShow("technicien"));
		} else {
			for (Technicien eachTech : technicians) {
				technicianList.addElement(eachTech.getNomUtilisateur());
			}
			supportList.setModel(technicianList);
		}
	}

	private DefaultListModel<String> noContentToShow(String where) {
		DefaultListModel<String> noContent = new DefaultListModel<>();
		noContent.addElement("Il n'y a pas de: " + where);
		return noContent;
	}

	private void initComponents() {

		fileChooser = new JFileChooser();
		assignRequestComponent = new JList<>();
		requestArea = new JTextArea(5, 20);
		commentsArea = new JTextArea(5, 20);

		addCommentBox = new JTextField();
		availableRequestBox = new JList<>();
		buttonTakeRequest = new JButton(TAKE_REQUEST_LABEL);
		descriptionArea = new JTextArea(5, 20);
		supportList = new JList<>();
		dropDownFinalize = new JComboBox<>(new DefaultComboBoxModel<>(
				new String[] { PICK_STATUS_LABEL, STATUS_SUCCESS_LABEL, STATUS_GIVE_UP_LABEL }));
		dropDownCategories = new JComboBox<>(new DefaultComboBoxModel<>(
				new String[] { PICK_CATEGORY_LABEL, Categorie.POSTE_DE_TRAVAIL.getValueString(),
						Categorie.SERVEUR.getValueString(), Categorie.SERVICE_WEB.getValueString(),
						Categorie.COMPTE_USAGER.getValueString(), Categorie.AUTRE.getValueString() }));
		fileLabel = new JLabel("(pas de fichier attaché)");

		JLabel assignedRequestsLabel = new JLabel("Les requêtes qui vous sont assignées:");
		JLabel selectedRequestLabel = new JLabel("Requête sélectionnée:");
		JLabel commentsRequestLabel = new JLabel("Commentaires:");
		JLabel addCommentLabel = new JLabel("Ajouter commentaire:");
		JLabel availableRequestLabel = new JLabel("Requêtes disponibles non-assignée:");

		JButton buttonNewRequest = new JButton("Créer une nouvelle requête");
		JButton buttonAssignRequest = new JButton("Assigner la requête à ce membre");
		JButton buttonAddFile = new JButton("Ajouter un fichier");
		JButton buttonShowRapport = new JButton("Afficher le rapport");
		JButton buttonQuit = new JButton("Quitter");

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		addCommentBox.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				addCommentToRequest(evt);
			}
		});

		assignRequestComponent.addListSelectionListener(evt -> {
			updateAssignedRequestComponent();
			if (assignRequestComponent.getSelectedIndex() >= 0) {
				updateCommentaireComponent();
			}
		});
		dropDownCategories.addItemListener(evt -> changeCategory());
		availableRequestBox.addListSelectionListener(evt -> updateAvailableRequestComponent());

		JScrollPane assignRequestScrollPanel = new JScrollPane(assignRequestComponent);
		JScrollPane requestScrollPanel = new JScrollPane(requestArea);
		JScrollPane commentsScrollPanel = new JScrollPane(commentsArea);
		JScrollPane availableRequestScrollPanel = new JScrollPane(availableRequestBox);
		JScrollPane descriptionScrollPanel = new JScrollPane(descriptionArea);
		JScrollPane supportListScrollPanel = new JScrollPane(supportList);

		dropDownFinalize.addItemListener(evt -> updateStatut());
		buttonTakeRequest.addActionListener(evt -> assignSelfToThisRequest());
		buttonNewRequest.addActionListener(evt -> createNewRequest());
		buttonAssignRequest.addActionListener(evt -> assignRequestToAnotherTechnician());
		buttonAddFile.addActionListener(evt -> attachFileToRequest());
		buttonShowRapport.addActionListener(evt -> showReport());
		buttonQuit.addActionListener(evt -> hitQuitButton());

		fileLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				openFileOnClick();
			}
		});

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addGroup(layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(availableRequestLabel)
								.addGroup(layout.createSequentialGroup()
										.addComponent(availableRequestScrollPanel, GroupLayout.PREFERRED_SIZE, 223,
												GroupLayout.PREFERRED_SIZE)
										.addGap(32, 32, 32)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(descriptionScrollPanel).addComponent(buttonTakeRequest))
										.addGap(18, 18, 18)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
												.addComponent(supportListScrollPanel, GroupLayout.Alignment.LEADING, 0,
														0, Short.MAX_VALUE)
												.addComponent(buttonAssignRequest, GroupLayout.Alignment.LEADING))))
								.addGap(86, 86, 86))
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(assignRequestScrollPanel, GroupLayout.DEFAULT_SIZE, 208,
												Short.MAX_VALUE)
										.addComponent(assignedRequestsLabel, GroupLayout.Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(selectedRequestLabel)
										.addComponent(requestScrollPanel, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(dropDownCategories, GroupLayout.PREFERRED_SIZE, 142,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(dropDownFinalize, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(21, 21, 21)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(commentsScrollPanel, GroupLayout.PREFERRED_SIZE, 289,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(commentsRequestLabel)
												.addGroup(layout.createSequentialGroup().addComponent(addCommentLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(addCommentBox)))
										.addGroup(layout.createSequentialGroup().addComponent(buttonAddFile)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(fileLabel)))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31,
										Short.MAX_VALUE)))
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addComponent(buttonQuit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(buttonShowRapport, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(buttonNewRequest, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(51, 51, 51).addComponent(buttonNewRequest)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(buttonShowRapport)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(buttonQuit))
						.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(assignedRequestsLabel)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(assignRequestScrollPanel, GroupLayout.DEFAULT_SIZE, 171,
												Short.MAX_VALUE)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
												.addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
														.addComponent(commentsRequestLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(commentsScrollPanel, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(layout
																.createParallelGroup(GroupLayout.Alignment.BASELINE)
																.addComponent(addCommentLabel)
																.addComponent(addCommentBox, GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE))
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addGroup(layout
																.createParallelGroup(GroupLayout.Alignment.BASELINE)
																.addComponent(buttonAddFile).addComponent(fileLabel)))
												.addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
														.addComponent(selectedRequestLabel)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(requestScrollPanel, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(dropDownCategories, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(dropDownFinalize, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
								.addGap(3, 3, 3)))
				.addGap(38, 38, 38).addComponent(availableRequestLabel)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(availableRequestScrollPanel, GroupLayout.PREFERRED_SIZE, 154,
								GroupLayout.PREFERRED_SIZE)
						.addGroup(layout.createSequentialGroup().addComponent(buttonTakeRequest)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(descriptionScrollPanel, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addComponent(buttonAssignRequest)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(supportListScrollPanel, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
				.addContainerGap()));

		pack();
	}

	private void hitQuitButton() {
		System.exit(0);
	}

	private void changeCategory() {
		if (assignRequestComponent.getSelectedIndex() < 0 || dropDownCategories.getSelectedIndex() <= 0)
			return;

		assignedRequests.get(assignRequestComponent.getSelectedIndex())
				.setCategorie(Categorie.fromString((String) dropDownCategories.getSelectedItem()));

		controller.updateRequest(assignedRequests.get(assignRequestComponent.getSelectedIndex()));
		updateAssignedRequestComponent();
	}

	private void assignSelfToThisRequest() {
		if (availableRequestBox.getSelectedIndex() >= 0) {
			Requete selectedRequest = availableRequests.get(availableRequestBox.getSelectedIndex());

			controller.assignTechToRequest(availableRequests.get(availableRequestBox.getSelectedIndex()), utilisateur);
			assignedRequests = utilisateur.getListeRequetes();

			assignedRequestsList = new DefaultListModel<>();
			for (Requete request : assignedRequests) {
				assignedRequestsList.addElement(request.getSujet());
			}
			assignRequestComponent.setModel(assignedRequestsList);

			availableRequestList.remove(availableRequestBox.getSelectedIndex());
			availableRequests.remove(selectedRequest);

			updateList();
		}

	}

	private void addCommentToRequest(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			controller.addCommentToRequest(selectedRequest, new Commentaire(addCommentBox.getText(), utilisateur));
			String s = "";
			for (Commentaire commentaire : selectedRequest.getListeCommentaires()) {
				s += commentaire.toString();
			}
			commentsArea.setText(s);
			addCommentBox.setText("");
		}
	}

	private void attachFileToRequest() {
		fileChooser.showOpenDialog(null);
		if (selectedRequest == null)
			return;

		if (fileChooser.getSelectedFile() == null)
			return;

		if (!fileChooser.getSelectedFile().exists())
			return;

		controller.fileUpload(fileChooser.getSelectedFile(), this.getClass().getName(), selectedRequest);
		updateAssignedRequestComponent();
	}

	private void openFileOnClick() {
		try {
			Requete request = assignedRequests.get(assignRequestComponent.getSelectedIndex());
			Desktop desktop = java.awt.Desktop.getDesktop();
			desktop.open(request.getFile());
		} catch (IOException ex) {
			Logger.getLogger(TechFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void updateStatut() {
		if (assignRequestComponent.getSelectedIndex() >= 0 && dropDownFinalize.getSelectedIndex() > 0) {
			assignedRequests.get(assignRequestComponent.getSelectedIndex())
					.setStatut(Statut.fromString((String) dropDownFinalize.getSelectedItem()));

			controller.updateRequest(assignedRequests.get(assignRequestComponent.getSelectedIndex()));
			updateAssignedRequestComponent();
		}
	}

	private void assignRequestToAnotherTechnician() {
		if (availableRequestBox.getSelectedIndex() >= 0) {
			if (supportList.getSelectedIndex() != -1) {
				controller.assignTechToRequest(availableRequests.get(availableRequestBox.getSelectedIndex()),
						technicians.get(supportList.getSelectedIndex()));
				availableRequestList.remove(availableRequestBox.getSelectedIndex());
				updateList();
			}
		}
	}

	private void updateList() {
		assignedRequests = controller.getAssignedRequests(utilisateur.getId());
		availableRequests = controller.getAvailableRequests();
		technicians = controller.getTechniciens();
		technicians.remove(utilisateur);

		assignedRequestsList = new DefaultListModel<>();
		makeAssignedRequestList();

		availableRequestList = new DefaultListModel<>();
		makeAvailableResquestList();

		technicianList = new DefaultListModel<>();
		makeTechnicianList();
	}

	private void createNewRequest() {
		this.setVisible(false);
		new RequeteFrame(utilisateur, this);
	}

	private void showReport() {
		RapportFrame rapp = new RapportFrame(new Rapport().makeRapport());
		rapp.setVisible(true);
	}

	private void updateAssignedRequestComponent() {

		assignedRequests = controller.getAssignedRequests(utilisateur.getId());

		if (assignRequestComponent.getSelectedIndex() >= 0) {

			selectedRequest = assignedRequests.get(assignRequestComponent.getSelectedIndex());

			requestArea.setText("Sujet: " + selectedRequest.getSujet() + "\nDescription: "
					+ selectedRequest.getDescription() + "\nCatégorie: " + selectedRequest.getCategorie().toString()
					+ "\nStatut: " + selectedRequest.getStatut().toString());

			if (selectedRequest.getFile() != null && selectedRequest.getFile().exists()) {
				fileLabel.setVisible(true);
				fileLabel.setText("Afficher :" + selectedRequest.getFile().getPath());
			} else {
				fileLabel.setVisible(false);
			}
		} else {
			requestArea.setText("");
		}

		if (selectedRequest.getStatut() == Statut.EN_TRAITEMENT) {
			dropDownCategories.setEnabled(true);
			dropDownFinalize.setEnabled(true);
		} else {
			dropDownCategories.setEnabled(false);
			dropDownFinalize.setEnabled(false);
		}
	}

	private void updateAvailableRequestComponent() {
		if (availableRequests.isEmpty())
			return;

		availableRequests = controller.getAvailableRequests();

		if (availableRequestBox.getSelectedIndex() < 0) {
			descriptionArea.setText("");
			return;
		}

		selectedRequest = availableRequests.get(availableRequestBox.getSelectedIndex());

		descriptionArea.setText("Sujet: " + selectedRequest.getSujet() + "\nDescription: "
				+ selectedRequest.getDescription() + "\nCatégorie: " + selectedRequest.getCategorie().toString()
				+ "\nStatut: " + selectedRequest.getStatut().toString());
	}

	private void updateCommentaireComponent() {
		if (selectedRequest == null)
			return;
		if (assignRequestComponent.getSelectedIndex() < 0)
			return;

		selectedRequest = assignedRequests.get(assignRequestComponent.getSelectedIndex());
		String s = "";
		for (Commentaire c : selectedRequest.getListeCommentaires()) {
			s += c.toString();
		}
		commentsArea.setText(s);
		addCommentBox.setText("");
	}
}
