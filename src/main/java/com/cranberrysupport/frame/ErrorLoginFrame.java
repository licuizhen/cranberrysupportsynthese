package com.cranberrysupport.frame;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class ErrorLoginFrame extends JFrame {

	private JLabel errorMessageLabel;
	private JButton goBackLabel;
	private GroupLayout layout;

	public ErrorLoginFrame() {
		initComponents();
		this.setVisible(true);
	}

	private void initComponents() {
		errorMessageLabel = new JLabel(WRONG_CREDENTIAL);
		goBackLabel = new JButton(OK);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		goBackLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				returnMainMenu();
			}
		});

		layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		createHorizontalGroup();
		createVerticalGroup();
		pack();
	}

	private void createHorizontalGroup() {
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GAP_81, Short.MAX_VALUE)
								.addComponent(errorMessageLabel).addGap(GAP_72, GAP_72, GAP_72))
				.addGroup(layout.createSequentialGroup().addGap(GAP_184, GAP_184, GAP_184).addComponent(goBackLabel)
						.addContainerGap(GAP_196, Short.MAX_VALUE)));
	}

	private void createVerticalGroup() {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(GAP_22, GAP_22, GAP_22).addComponent(errorMessageLabel)
						.addGap(GAP_18, GAP_18, GAP_18).addComponent(goBackLabel)
						.addContainerGap(GAP_27, Short.MAX_VALUE)));
	}

	private void returnMainMenu() {
		this.setVisible(false);
		new MainMenuFrame();
	}
}
