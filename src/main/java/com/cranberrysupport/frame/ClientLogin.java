package com.cranberrysupport.frame;

import com.cranberrysupport.controller.Controller;
import com.cranberrysupport.entity.Client;
import com.cranberrysupport.entity.Utilisateur;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class ClientLogin extends JFrame {

	private Controller controller = Controller.getInstance();

	private JTextField passwordInput;
	private JTextField usernameInput;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JLabel usernamePlaceHolderLabel;
	private JLabel passwordPlaceHolderLabel;
	private JButton buttonOK;
	private JButton buttonCancel;

	private GroupLayout layout;

	public ClientLogin() {
		initComponents();
		setVisible(true);
	}

	private void initComponents() {
		usernameLabel = new JLabel(USERNAME_LABEL);
		passwordLabel = new JLabel(PASSWORD_LABEL);
		usernamePlaceHolderLabel = new JLabel(USERNAME_EXAMPLE);
		passwordPlaceHolderLabel = new JLabel(PASSWORD_EXAMPLE);

		usernameInput = new JTextField();
		passwordInput = new JTextField();
		buttonOK = new JButton(OK);
		buttonCancel = new JButton(CANCEL);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		buttonOK.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				okBtnMouseClicked();
			}
		});

		buttonCancel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				annulerBtnMouseClicked();
			}
		});

		layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		createHorizontalLayout();
		createVerticalLayout();

		pack();
	}

	private void createVerticalLayout() {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(GAP_22, GAP_22, GAP_22)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(usernameLabel)
								.addComponent(usernameInput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(usernamePlaceHolderLabel))
						.addGap(GAP_18, GAP_18, GAP_18)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(passwordLabel)
								.addComponent(passwordInput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(passwordPlaceHolderLabel))
						.addGap(GAP_18, GAP_18, GAP_18)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(buttonCancel)
								.addComponent(buttonOK))
						.addContainerGap(GAP_22, Short.MAX_VALUE)));
	}

	private void createHorizontalLayout() {
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup().addContainerGap()
								.addComponent(buttonOK, GroupLayout.PREFERRED_SIZE, GAP_69, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(buttonCancel))
						.addGroup(
								layout.createSequentialGroup().addGap(GAP_35, GAP_35, GAP_35)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(usernameLabel).addComponent(passwordLabel))
										.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(passwordInput, GroupLayout.DEFAULT_SIZE, GAP_149,
														Short.MAX_VALUE)
												.addComponent(usernameInput, GroupLayout.DEFAULT_SIZE, GAP_149,
														Short.MAX_VALUE))))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(passwordPlaceHolderLabel).addComponent(usernamePlaceHolderLabel))
						.addContainerGap()));
	}

	private void okBtnMouseClicked() {
		Utilisateur potentiel = controller.getUser(usernameInput.getText());

		if (potentiel == null || !isClientCanLog(potentiel)) {
			new ErrorLoginFrame();
		} else {
			new ClientFrame(potentiel);
		}
		this.setVisible(false);
	}

	private boolean isClientCanLog(Utilisateur potentiel) {
		return controller.isLoginValid(potentiel, passwordInput.getText())
				&& potentiel.getRole().equals(Client.class.getSimpleName().toLowerCase());
	}

	private void annulerBtnMouseClicked() {
		this.setVisible(false);
		new MainMenuFrame();
	}

}
