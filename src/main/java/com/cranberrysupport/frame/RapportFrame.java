package com.cranberrysupport.frame;

import javax.swing.*;
import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class RapportFrame extends JFrame {

	private JTextArea rapportArea;

	public RapportFrame(String rapport) {
		initComponents();
		rapportArea.setText(rapport);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	private void initComponents() {

		JScrollPane panel = new JScrollPane();
		JButton button = new JButton(GO_BACK_LABEL);
		rapportArea = new JTextArea(5, 20);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		rapportArea.setEditable(false);
		rapportArea.setLineWrap(true);
		panel.setViewportView(rapportArea);

		button.addActionListener(evt -> setVisible(false));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addComponent(panel,
										GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup().addGap(89, 89, 89).addComponent(button)))
						.addContainerGap(22, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 457, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(button)
						.addContainerGap(18, Short.MAX_VALUE)));

		pack();
	}
}
