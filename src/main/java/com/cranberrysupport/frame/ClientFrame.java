package com.cranberrysupport.frame;

import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.*;

import com.cranberrysupport.controller.Controller;
import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Utilisateur;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class ClientFrame extends JFrame {

	private static final String NO_REQUEST_YET = "Vous n'avez pas de requête encore.";

	private Controller controller = Controller.getInstance();

	private Utilisateur utilisateur;
	private List<Requete> requests;
	private Requete selectedRequest;
	private boolean showComments = false;

	private JTextArea commentArea;
	private JButton buttonChooseFile;
	private JFileChooser fileChooser;
	private JList<String> requestsComponent;
	private JButton buttonModifyRequest;
	private JTextArea requestArea;
	private JButton buttonSeeComments;
	private GroupLayout layout;

	public ClientFrame(Utilisateur utilisateur) {
		initComponents();
		this.setVisible(true);
		this.utilisateur = utilisateur;
		requests = controller.getUserRequest(utilisateur);

		createRequestList();
		commentArea.setVisible(false);
	}

	private void createRequestList() {
		DefaultListModel<String> requestList = new DefaultListModel<>();

		if (requests.isEmpty()) {
			requestList.addElement(NO_REQUEST_YET);
		} else {
			for (Requete request : requests) {
				requestList.addElement(request.getSujet());
			}

		}
		requestsComponent.setModel(requestList);
	}

	private void initComponents() {
		JLabel requetesLbl = new JLabel(YOUR_REQUESTS);
		JButton addRequeteBtn = new JButton(MAKE_NEW_REQUEST);
		JButton quitBtn = new JButton(QUIT);
		JLabel jLabel1 = new JLabel(EDIT_THE_REQUEST);

		fileChooser = new JFileChooser();
		requestsComponent = new JList<>();
		buttonModifyRequest = new JButton(ADD_COMMENT);
		buttonChooseFile = new JButton(ADD_FILE);
		buttonSeeComments = new JButton(SEE_COMMENTS);
		requestArea = new JTextArea(5, 20);
		commentArea = new JTextArea(5, 20);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		requestsComponent.addListSelectionListener(evt -> updateInformationInOtherBoxes());

		addRequeteBtn.addActionListener(evt -> buttonAddRequest());

		quitBtn.addActionListener(evt -> buttonQuit());

		buttonModifyRequest.setEnabled(false);
		buttonModifyRequest.addActionListener(evt -> actionIsPerformed());

		buttonChooseFile.addActionListener(evt -> chooseFileToAddToRequest());

		requestArea.setEditable(false);
		requestArea.setLineWrap(true);

		buttonSeeComments.setBackground(new java.awt.Color(102, 153, 255));
		buttonSeeComments.addActionListener(evt -> showComments());

		commentArea.setLineWrap(true);
		commentArea.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				addingCommentToRequest(evt);
			}
		});

		JScrollPane jScrollPane1 = new JScrollPane(requestsComponent);
		JScrollPane jScrollPane2 = new JScrollPane(requestArea);
		JScrollPane jScrollPane3 = new JScrollPane(commentArea);

		layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		createHorizontalGroup(requetesLbl, addRequeteBtn, quitBtn, jLabel1, jScrollPane1, jScrollPane2, jScrollPane3,
				layout);
		createVerticalGroup(requetesLbl, addRequeteBtn, quitBtn, jLabel1, jScrollPane1, jScrollPane2, jScrollPane3,
				layout);
		pack();
	}

	private void createVerticalGroup(JLabel requetesLbl, JButton addRequeteBtn, JButton quitBtn, JLabel jLabel1,
			JScrollPane jScrollPane1, JScrollPane jScrollPane2, JScrollPane jScrollPane3, GroupLayout layout) {
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(GroupLayout.Alignment.LEADING,
								layout.createSequentialGroup().addGap(76, 76, 76).addComponent(jScrollPane2,
										GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addGap(22, 22, 22).addComponent(addRequeteBtn)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(requetesLbl)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 290,
														Short.MAX_VALUE)
												.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(quitBtn))
										.addGroup(layout.createSequentialGroup().addComponent(buttonSeeComments)
												.addGap(26, 26, 26).addComponent(jLabel1)
												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(buttonChooseFile)
												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(buttonModifyRequest)
												.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(jScrollPane3, GroupLayout.DEFAULT_SIZE, 197,
														Short.MAX_VALUE)))))
						.addContainerGap()));
	}

	private void createHorizontalGroup(JLabel requetesLbl, JButton addRequeteBtn, JButton quitBtn, JLabel jLabel1,
			JScrollPane jScrollPane1, JScrollPane jScrollPane2, JScrollPane jScrollPane3, GroupLayout layout) {
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(addRequeteBtn)
								.addComponent(requetesLbl)
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 214,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(quitBtn))
										.addGap(10, 10, 10)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(buttonSeeComments).addComponent(buttonChooseFile)
												.addComponent(buttonModifyRequest).addComponent(jLabel1)
												.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 166,
														GroupLayout.PREFERRED_SIZE))
										.addGap(9, 9, 9)
										.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)))
						.addContainerGap()));
	}

	private void buttonAddRequest() {
		this.setVisible(false);
		new RequeteFrame(utilisateur, this);
	}

	private void updateInformationInOtherBoxes() {
		updateRequete();
	}

	private void buttonQuit() {
		System.exit(0);
	}

	private void chooseFileToAddToRequest() {
		fileChooser.showOpenDialog(null);

		if (selectedRequest == null)
			return;

		if (fileChooser.getSelectedFile() == null)
			return;

		if (!fileChooser.getSelectedFile().exists())
			return;

		controller.fileUpload(fileChooser.getSelectedFile(), this.getClass().getName(), selectedRequest);
		updateRequete();
	}

	private void actionIsPerformed() {
		commentArea.setVisible(true);
		commentArea.requestFocus();
	}

	private void showComments() {
		if (selectedRequest != null) {
			if (!showComments) {
				String s = "";
				for (Commentaire c : selectedRequest.getListeCommentaires()) {
					s += c.toString();
				}
				requestArea.setText(s);
				showComments = true;
				buttonChooseFile.setEnabled(false);
				buttonModifyRequest.setEnabled(true);
			} else {
				updateRequete();
			}
		}
	}

	private void updateRequete() {
		selectedRequest = requests.get(requestsComponent.getSelectedIndex());
		requestArea.setText("Sujet: " + selectedRequest.getSujet() + "\nDescription: "
				+ selectedRequest.getDescription() + "\nCatégorie: " + selectedRequest.getCategorie().toString()
				+ "\nStatut: " + selectedRequest.getStatut().toString() + "\nFichier :"
				+ (selectedRequest.getFile() == null ? "Aucun" : selectedRequest.getFile().getPath()));
		showComments = false;
		buttonChooseFile.setEnabled(true);
		buttonModifyRequest.setEnabled(false);
	}

	private void addingCommentToRequest(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			controller.addCommentToRequest(selectedRequest, new Commentaire(commentArea.getText(), utilisateur));
			String comment = "";

			for (Commentaire commentaire : selectedRequest.getListeCommentaires()) {
				comment += commentaire.toString();
			}
			requestArea.setText(comment);
			commentArea.setVisible(false);
			buttonModifyRequest.requestFocus();
		}
	}
}
