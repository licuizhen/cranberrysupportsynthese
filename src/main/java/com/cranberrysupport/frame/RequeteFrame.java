package com.cranberrysupport.frame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import com.cranberrysupport.controller.Controller;
import com.cranberrysupport.entity.Categorie;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Utilisateur;

import static com.cranberrysupport.util.Variables.*;

@SuppressWarnings("serial")
public class RequeteFrame extends JFrame {

	public Controller controller = Controller.getInstance();

	private JComboBox<String> categoryArea;
	private JTextArea descriptionArea;
	private JFileChooser fileChooser;
	private JTextField file_pathname;
	private JTextField subjectArea;

	private Utilisateur utilisateur;
	private JFrame framePrevious;
	private File file;

	public RequeteFrame(Utilisateur utilisateur, JFrame pagePrecedente) {
		initComponents();
		this.setVisible(true);
		this.utilisateur = utilisateur;
		framePrevious = pagePrecedente;
	}

	private void initComponents() {

		JLabel titleLbl = new JLabel(NEW_REQUEST_LABEL);
		JLabel sujetLbl = new JLabel(SUBJECT_LABEL);
		JLabel catLbl = new JLabel(CATEGORY_LABEL);
		JButton doneBtn = new JButton(DONE_LABEL);
		JButton quitBtn = new JButton(GO_BACK_LABEL);
		JLabel descripLbl = new JLabel(DESCRIPTION_LABEL);
		JScrollPane descpScroll = new JScrollPane();
		JSeparator jSeparator1 = new JSeparator();
		JLabel fichierLbl = new JLabel(FILE_LABEL);
		JButton selPathBtn = new JButton(UPLOAD_FILE_LABEL);
		subjectArea = new JTextField();
		descriptionArea = new JTextArea(5, 20);
		file_pathname = new JTextField();
		fileChooser = new JFileChooser();
		categoryArea = new JComboBox<>();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		descriptionArea.setBorder(null);
		descpScroll.setViewportView(descriptionArea);

		file_pathname.addActionListener(evt -> file_pathname.setText(fileChooser.getSelectedFile().getPath()));
		selPathBtn.addActionListener(evt -> uploadFile());

		categoryArea.setModel(new DefaultComboBoxModel<>(
				new String[] { "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));

		doneBtn.addActionListener(evt -> saveRequest());
		quitBtn.addActionListener(evt -> goToPreviousFrame());

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
						.createSequentialGroup()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(titleLbl)
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(sujetLbl).addComponent(descripLbl)
												.addComponent(fichierLbl))
										.addGap(18, 18, 18)
										.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(file_pathname, GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(subjectArea, GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(descpScroll, GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(selPathBtn))))
						.addContainerGap(30, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addComponent(catLbl).addGap(63, 63, 63)
								.addComponent(categoryArea, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(176, Short.MAX_VALUE))
						.addGroup(GroupLayout.Alignment.TRAILING,
								layout.createSequentialGroup().addGap(124, 124, 124).addComponent(doneBtn)
										.addGap(18, 18, 18).addComponent(quitBtn)
										.addContainerGap(104, Short.MAX_VALUE))))
				.addComponent(jSeparator1, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap().addComponent(titleLbl).addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(sujetLbl)
						.addComponent(subjectArea, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(descripLbl)
						.addComponent(descpScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 105,
								javax.swing.GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(file_pathname, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(fichierLbl))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(selPathBtn)
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(categoryArea, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(catLbl))
				.addGap(27, 27, 27)
				.addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8,
						javax.swing.GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(quitBtn)
						.addComponent(doneBtn))
				.addContainerGap(31, Short.MAX_VALUE)));

		pack();
	}

	private void uploadFile() {
		fileChooser.showOpenDialog(framePrevious);

		if (fileChooser.getSelectedFile() == null) {
			return;
		}
		file_pathname.setText(fileChooser.getSelectedFile().getPath());

		if (!fileChooser.getSelectedFile().exists()) {
			return;
		}

		File uploadedFile = fileChooser.getSelectedFile();
		String pathname = fileChooser.getSelectedFile().getName();

		try {
			file = new File("dat/" + pathname);
			InputStream inputFile = new FileInputStream(uploadedFile);
			OutputStream outputFile = new FileOutputStream(file);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = inputFile.read(buffer)) > 0) {
				outputFile.write(buffer, 0, length);
			}
			inputFile.close();
			outputFile.close();
		} catch (IOException ex) {
			Logger.getLogger(RequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void saveRequest() {
		if (subjectArea.getText().isEmpty() || descriptionArea.getText().isEmpty()
				|| categoryArea.getSelectedItem() == null) {
			Logger.getLogger(RequeteFrame.class.getName()).config("Ajout d'une nouvelle requête non traité."
					+ "La requête contient un sujet, une description ou une catégorie vide.\n");
			return;
		}

		Requete request = new Requete(subjectArea.getText(), descriptionArea.getText().replaceAll("\n", " "),
				utilisateur, Categorie.fromString((String) categoryArea.getSelectedItem()));
		request.setFile(file);

		controller.addNewRequest(request);

		this.setVisible(false);
		if (utilisateur.getRole().equals("client")) {
			ClientFrame retourPageClient = new ClientFrame(utilisateur);
			retourPageClient.setVisible(true);
		} else {
			TechFrame retourPageTech = new TechFrame(utilisateur);
			retourPageTech.setVisible(true);
		}
	}

	private void goToPreviousFrame() {
		this.setVisible(false);
		framePrevious.setVisible(true);
	}
}