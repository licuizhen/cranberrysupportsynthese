package com.cranberrysupport;

import java.util.logging.Logger;

import com.cranberrysupport.frame.MainMenuFrame;
import com.cranberrysupport.service.PeuplerService;

public class CranberrySupportApplication {

    private static final Logger logger = Logger.getLogger(MainMenuFrame.class.getName());

    public static void main(String[] args) {
        PeuplerService.run();

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            logger.log(java.util.logging.Level.SEVERE, null, e);
        }

        java.awt.EventQueue.invokeLater(() -> new MainMenuFrame().setVisible(true));
    }
}
