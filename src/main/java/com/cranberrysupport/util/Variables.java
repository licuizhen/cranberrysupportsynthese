package com.cranberrysupport.util;

public class Variables {
	public static final String WRONG_CREDENTIAL = "Désolé, votre nom d'utilisateur ou votre mot de passe est incorrect.";
	public static final String STARTING = "Demarrage";
	public static final String CHOOSING_LOGIN_TYPE = "Démarrer l'application en tant que:";
	public static final String YOUR_REQUESTS = "Vos requêtes:";
	public static final String MAKE_NEW_REQUEST = "Faire une nouvelle requête";
	public static final String EDIT_THE_REQUEST = "Modifier la requête sélectionnée:";
	public static final String ADD_COMMENT = "Ajouter un commentaire";
	public static final String ADD_FILE = "Ajouter un fichier";
	public static final String SEE_COMMENTS = "Voir les commentaires";

	public static final String CLIENT = "Utilisateur";
	public static final String TECHNICIAN = "Technicien";

	public static final String USERNAME_LABEL = "Nom d'utilisateur:";
	public static final String PASSWORD_LABEL = "Mot de passe:";
	public static final String USERNAME_EXAMPLE = "ex.: Isabeau Desrochers";
	public static final String PASSWORD_EXAMPLE = "ex.: MonMdp";
	public static final String UPLOAD_FILE_LABEL = "Téléverser un fichier";
	public static final String FILE_LABEL = "Fichier:";
	public static final String DESCRIPTION_LABEL = "Description:";
	public static final String GO_BACK_LABEL = "Revenir";
	public static final String DONE_LABEL = "Terminer";
	public static final String CATEGORY_LABEL = "Catégorie:";
	public static final String SUBJECT_LABEL = "Sujet de la requête:";
	public static final String NEW_REQUEST_LABEL = "Nouvelle requête";
	public static final String PICK_CATEGORY_LABEL = "Changer la catégorie";
	public static final String STATUS_GIVE_UP_LABEL = "Abandon";
	public static final String STATUS_SUCCESS_LABEL = "Succès";
	public static final String PICK_STATUS_LABEL = "Choisir un statut";
	public static final String TAKE_REQUEST_LABEL = "Prendre la requête sélectionnée";

	public static final String OK = "OK";
	public static final String CANCEL = "Annuler";
	public static final String QUIT = "Quitter";

	public static final int GAP_0 = 0;
	public static final int GAP_18 = 18;
	public static final int GAP_22 = 22;
	public static final int GAP_27 = 27;
	public static final int GAP_35 = 35;
	public static final int GAP_42 = 42;
	public static final int GAP_69 = 69;
	public static final int GAP_72 = 72;
	public static final int GAP_81 = 81;
	public static final int GAP_149 = 149;
	public static final int GAP_184 = 184;
	public static final int GAP_196 = 196;
	public static final int GAP_300 = 300;
	public static final int GAP_400 = 400;

}
