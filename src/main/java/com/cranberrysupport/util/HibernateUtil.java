package com.cranberrysupport.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.cranberrysupport.entity.Client;
import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().addAnnotatedClass(Utilisateur.class).addAnnotatedClass(Client.class)
					.addAnnotatedClass(Technicien.class).addAnnotatedClass(Requete.class)
					.addAnnotatedClass(Commentaire.class).buildSessionFactory();

		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

}
