package com.cranberrysupport;

import com.cranberrysupport.entity.Client;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private UserService userService = Mockito.mock(UserService.class);

    @Test
    public void checkLoginPasswordValid() {
        Utilisateur utilisateur = new Client("a", "b", "al", "al");
        Mockito.when(userService.isLoginValid(utilisateur, "al")).thenReturn(true);

        assertTrue(userService.isLoginValid(utilisateur, "al"));
    }

    @Test
    public void checkLoginPasswordInvalid() {
        Utilisateur utilisateur = new Client("a", "b", "al", "al");
        Mockito.when(userService.isLoginValid(utilisateur, "abc")).thenReturn(false);

        assertFalse(userService.isLoginValid(utilisateur, "abc"));
    }

    @Test
    public void checkLoginIsNull() {
        Utilisateur utilisateur = new Client("a", "b", "al", "al");
        Mockito.when(userService.isLoginValid(utilisateur, null)).thenReturn(false);

        assertFalse(userService.isLoginValid(utilisateur, null));
    }

    @Test
    public void gettingSpecificTechnician() {
        Technicien technicien = new Technicien("a", "a", "ge", "ge");
        technicien.setId(123L);

        Mockito.when(userService.getTechnicien(123L)).thenReturn(technicien);

        assertEquals(userService.getTechnicien(123L), technicien);
    }

    @Test
    public void gettingTechnicianList() {
        Technicien technicien1 = new Technicien("a", "a", "ge", "ge");
        Technicien technicien2 = new Technicien("b", "b", "gea", "gea");
        Technicien technicien3 = new Technicien("c", "c", "geb", "geb");
        List<Technicien> techList = Arrays.asList(technicien1, technicien2, technicien3);

        Mockito.when(userService.getTechniciens()).thenReturn(techList);
        assertEquals(userService.getTechniciens(), techList);
    }

    @Test
    public void gettingUser() {
        Utilisateur client = new Client("a", "b", "al", "al");
        Utilisateur tech = new Technicien("a", "b", "bc", "bc");

        Mockito.when(userService.getUser("al")).thenReturn(client);
        Mockito.when(userService.getUser("bc")).thenReturn(tech);

        assertEquals(userService.getUser("bc"), tech);
        assertEquals(userService.getUser("al"), client);

    }
}
