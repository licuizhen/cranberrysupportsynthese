package com.cranberrysupport;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.cranberrysupport.entity.Categorie;
import com.cranberrysupport.entity.Client;
import com.cranberrysupport.entity.Commentaire;
import com.cranberrysupport.entity.Requete;
import com.cranberrysupport.entity.Statut;
import com.cranberrysupport.entity.Technicien;
import com.cranberrysupport.entity.Utilisateur;
import com.cranberrysupport.service.RequestService;

public class RequestServiceTest {
	private static Utilisateur user;
	private static Utilisateur user2;
	private static List<Requete> requetes = new ArrayList<>();
	private static List<Requete> requetes2 = new ArrayList<>();
	private static RequestService rs = Mockito.mock(RequestService.class);

	@Before
	public void setUp() throws Exception {
		user = new Client("a", "a", "a", "a");
		requetes.add(new Requete("sub", "desc", user, Categorie.AUTRE));
		requetes.add(new Requete("sub2", "desc2", user, Categorie.SERVEUR));
		user2 = new Technicien("t", "t", "t", "t");
		requetes2.add(new Requete("sub", "desc", user2, Categorie.AUTRE));
		requetes2.add(new Requete("sub2", "desc2", user2, Categorie.SERVEUR));
		requetes.get(0).setTechnicien(user2);
		requetes2.get(0).setTechnicien(user2);
	}

	@After
	public void tearDown() throws Exception {
		user = null;
		user2 = null;
		requetes.clear();
		requetes2.clear();
	}

	@Test
	public void getUserRequestTest() {
		Mockito.when(rs.getUserRequest(user)).thenReturn(requetes);
		assertTrue(rs.getUserRequest(user).size() == 2);
	}

	@Test
	public void getRequestByTechnicianTest() {
		Mockito.when(rs.getRequestByTechnician(user2)).thenReturn(requetes2);
		assertTrue(rs.getRequestByTechnician(user2).size() == 2);
	}

	@Test
	public void addCommentToRequestTest() {
		Commentaire c = new Commentaire("ok", user2);
		Requete r = requetes2.get(0);
		r.addCommentaire(c);
		rs.addCommentToRequest(r, c);
		assertTrue(requetes2.get(0).getListeCommentaires().size() == 1);
	}

	@Test
	public void getAssignedRequestsTest() {
		List<Requete> r = new ArrayList<>();
		r.add(requetes.get(0));
		r.add(requetes2.get(0));
		Mockito.when(rs.getAssignedRequests(1L)).thenReturn(r);
		assertTrue(rs.getAssignedRequests(1L).size() == 2);
	}

	@Test
	public void assignTechToRequestTest() {
		Requete r = mock(Requete.class);
		RequestService rs = mock(RequestService.class);
		doNothing().when(rs).assignTechToRequest(r, user2);
		rs.assignTechToRequest(r, user2);
		verify(rs, times(1)).assignTechToRequest(r, user2);
	}

	@Test
	public void getAvailableRequestsTest() {
		requetes.addAll(requetes2);
		Mockito.when(rs.getAvailableRequests()).thenReturn(requetes);
		for (Requete r : requetes) {
			assertTrue(r.getStatut().equals(Statut.OUVERT));
		}
	}

	@Test
	public void addNewRequestTest() {
		RequestService rs = mock(RequestService.class);
		Requete r = mock(Requete.class);
		r.setClient(user);
		rs.addNewRequest(r);
		verify(rs).addNewRequest(r);
	}

	@Test
	public void updateFileTest() {
		RequestService rs = mock(RequestService.class);
		File file = mock(File.class);
		Requete r = mock(Requete.class);
		r.setFile(file);
		rs.updateFile(r);
		verify(rs).updateFile(r);
	}
}
