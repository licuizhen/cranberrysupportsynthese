Pour voir les commits, cliquez sur le lien suivant :
https://bitbucket.org/licuizhen/cranberrysupportsynthese/src/master/

Pour vous connecter afin d'essayer le logiciel, vous pouvez prendre l'un des comptes suivants:

Un client:
Nom d'utilisateur: al
Mot de passe: al

Un technicien:
Nom d'utilisateur: bc
Mot de passe: bc

Il faut modifier dans le fichier hibernate.cfg.xml la ligne suivante :
	<property name="hbm2ddl.auto">create</property>
Après le premier lancement pour la ligne suivant :
	<property name="hbm2ddl.auto">update</property>